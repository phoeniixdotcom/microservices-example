CREATE TABLE example
(
    id BIGINT PRIMARY KEY NOT NULL IDENTITY,
    content VARCHAR(255)
);
CREATE UNIQUE INDEX example_id_uindex ON example (id);