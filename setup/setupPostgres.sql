-- Create microservices-example database
CREATE DATABASE "microservices-example"
  WITH OWNER = postgres
  ENCODING = 'UTF8'
  TABLESPACE = pg_default
  LC_COLLATE = 'en_US.UTF-8'
  LC_CTYPE = 'en_US.UTF-8'
  CONNECTION LIMIT = -1;

-- Setup public schema
-- CREATE SCHEMA public
--   AUTHORIZATION postgres;

-- GRANT ALL ON SCHEMA public TO postgres;
-- GRANT ALL ON SCHEMA public TO public;
-- COMMENT ON SCHEMA public
-- IS 'standard public schema';

-- Setup Example Table
CREATE TABLE public.example
(
  id bigint NOT NULL,
  content character varying NOT NULL,
  CONSTRAINT example_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.example
  OWNER TO postgres;

-- Create


-- Read
SELECT * FROM public.example;

--Update


--Delete