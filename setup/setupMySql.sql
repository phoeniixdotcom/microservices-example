CREATE DATABASE `microservices-example` /*!40100 DEFAULT CHARACTER SET latin1 */;

CREATE TABLE `example` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;