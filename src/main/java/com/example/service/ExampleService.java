package com.example.service;

import com.example.config.ExampleMessageResource;
import com.example.domain.ExampleResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class ExampleService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ExampleMessageResource exampleMessageResource;

    @Value("${db.driverClass}")
    private String driver;
    @Value("${db.connectionUrl}")
    private String url;
    @Value("${db.username}")
    private String username;
    @Value("${db.password}")
    private String password;

    private Properties properties;

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    public ExampleResponse index(String name) throws Exception {
        ExampleResponse response = null;
        final String template = exampleMessageResource.getMessage("application.welcome") + ", %s!";
        try {
            logger.debug("Attempting to process param endpoint.");
            return new ExampleResponse(counter.incrementAndGet(),
                    String.format(template, name));
        } catch (Exception exception) {
            logger.debug("Processor failed.", exception);
        }
        return response;
    }

}