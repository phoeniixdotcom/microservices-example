package com.example.service;

import com.example.config.ExampleMessageResource;
import com.example.domain.ExampleRequest;
import com.example.domain.ExampleResponse;
import com.example.mappers.ExampleMapperClass;
import com.example.mybatis.entity.ExamplePojo;
import org.apache.ibatis.datasource.pooled.PooledDataSourceFactory;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.Properties;

@Service
public class ExampleServiceJdbc {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ExampleMessageResource exampleMessageResource;

    @Value("${db.driverClass}")
    private String driver;
    @Value("${db.connectionUrl}")
    private String url;
    @Value("${db.username}")
    private String username;
    @Value("${db.password}")
    private String password;

    private Properties properties = new Properties();

    /**
     * Read
     * @param id
     * @return
     * @throws Exception
     */
    public ExampleResponse read(long id) throws Exception {
        ExampleResponse response = null;
        try {
            logger.debug("Attempting to process body endpoint");

            ExamplePojo examplePojo;

            PooledDataSourceFactory pooledDataSourceFactory = new PooledDataSourceFactory();
            pooledDataSourceFactory.setProperties(getDbProperties());
            DataSource dataSource = pooledDataSourceFactory.getDataSource();

            TransactionFactory transactionFactory = new JdbcTransactionFactory();
            Environment environment = new Environment("example", transactionFactory, dataSource);
            Configuration configuration = new Configuration(environment);
            configuration.addMapper(ExampleMapperClass.class);
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);
            SqlSession session = sqlSessionFactory.openSession();

            try {
                ExampleMapperClass exampleMapperClass = session.getMapper(ExampleMapperClass.class);
                examplePojo = exampleMapperClass.getExamplePojo(id);
            } finally {
                session.close();
            }

            response = new ExampleResponse(examplePojo.getId(), examplePojo.getContent());
        } catch (Exception exception) {
            logger.debug("Processor failed.", exception);
        }
        return response;
    }

    /**
     * Update
     * @param request
     * @return
     * @throws Exception
     */
    public ExampleResponse update(ExampleRequest request) throws Exception {
        ExampleResponse response = null;
        try {
            logger.debug("Attempting to process body endpoint");

            ExamplePojo examplePojo;

            PooledDataSourceFactory pooledDataSourceFactory = new PooledDataSourceFactory();
            pooledDataSourceFactory.setProperties(getDbProperties());
            DataSource dataSource = pooledDataSourceFactory.getDataSource();

            TransactionFactory transactionFactory = new JdbcTransactionFactory();
            Environment environment = new Environment("example", transactionFactory, dataSource);
            Configuration configuration = new Configuration(environment);
            configuration.addMapper(ExampleMapperClass.class);
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);
            SqlSession session = sqlSessionFactory.openSession();

            try {
                ExampleMapperClass exampleMapperClass = session.getMapper(ExampleMapperClass.class);
                examplePojo = exampleMapperClass.setExamplePojo(request.getId(), request.getContent());
            } finally {
                session.close();
            }

            response = new ExampleResponse(examplePojo.getId(), examplePojo.getContent());
        } catch (Exception exception) {
            logger.debug("Processor failed.", exception);
        }
        return response;
    }

    private Properties getDbProperties(){
        properties.setProperty("driver", driver);
        properties.setProperty("url", url);
        properties.setProperty("username", username);
        properties.setProperty("password", password);
        return properties;
    }
}