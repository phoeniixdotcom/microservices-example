package com.example.controller;

import com.example.config.ExampleMessageResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ExampleController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private ExampleMessageResource exampleMessageResource;

    @RequestMapping("/")
    public String index() {
        return exampleMessageResource.getMessage("application.welcome");
    }

    @RequestMapping("/check")
    public boolean check() throws Exception {
        return true;
    }

//


}
