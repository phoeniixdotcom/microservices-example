package com.example.controller;

import com.example.config.ExampleMessageResource;
import com.example.domain.ExampleRequest;
import com.example.domain.ExampleResponse;
import com.example.service.ExampleServiceMyBatis;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ExampleControllerMyBatis {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ExampleServiceMyBatis exampleServiceMyBatis;

    @RequestMapping(value = "/exampleGetMyBatis", method = RequestMethod.GET)
    public ExampleResponse exampleParam(@RequestParam(value="name", defaultValue="World") String name) {
        ExampleResponse response = null;
        try {
            response = exampleServiceMyBatis.index(name);
        } catch (Exception e) {
            logger.error("Unable to process Example Request", e);
        }
        return response;
    }

    @RequestMapping(value = "/exampleGetRecordMyBatis", method = RequestMethod.POST)
    public ExampleResponse exampleGetRecordMyBatis(@RequestBody ExampleRequest request) {
        ExampleResponse response = null;
        try {
            response = exampleServiceMyBatis.getRecordMyBatis(request);
        } catch (Exception e) {
            logger.error("Unable to process Example Request", e);
        }
        return response;
    }
    
}
