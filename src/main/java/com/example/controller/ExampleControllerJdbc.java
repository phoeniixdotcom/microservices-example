package com.example.controller;

import com.example.config.ExampleMessageResource;
import com.example.domain.ExampleRequest;
import com.example.domain.ExampleResponse;
import com.example.service.ExampleServiceJdbc;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ExampleControllerJdbc {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ExampleServiceJdbc exampleServiceJdbc;

    // Create

    // Read
    @RequestMapping(value = "/exampleRead", method = RequestMethod.GET)
    @ApiOperation(value = "getGreeting", nickname = "getGreeting")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "User's name", required = false, dataType = "string", paramType = "query", defaultValue="Niklas")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    public ExampleResponse exampleRead(@RequestParam(value="id") long id) { //@RequestParam(value="name", defaultValue="World"
        ExampleResponse response = null;
        try {
            response = exampleServiceJdbc.read(id);
        } catch (Exception e) {
            logger.error("Unable to process Example Request", e);
        }
        return response;
    }

    // Update
    @RequestMapping(value = "/exampleUpdate", method = RequestMethod.POST)
    public ExampleResponse exampleUpdate(@RequestBody ExampleRequest request) {
        ExampleResponse response = null;
        try {
            response = exampleServiceJdbc.update(request);
        } catch (Exception e) {
            logger.error("Unable to process Example Request", e);
        }
        return response;
    }

    // Delete

}
